// checkTokenVK project main.go
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

var client = http.Client{
	Timeout: time.Duration(60 * time.Second),
}

type VkCurUser struct {
	Response []struct {
		ID              int    `json:"id"`
		FirstName       string `json:"first_name"`
		LastName        string `json:"last_name"`
		IsClosed        bool   `json:"is_closed"`
		CanAccessClosed bool   `json:"can_access_closed"`
	} `json:"response"`
	Error struct {
		ErrorCode     int    `json:"error_code"`
		ErrorMsg      string `json:"error_msg"`
		RequestParams []struct {
			Key   string `json:"key"`
			Value string `json:"value"`
		} `json:"request_params"`
	} `json:"error"`
}

func main() {
	for {
		var token string = "q"
		var vkUser = VkCurUser{}
		fmt.Println("-= Check Token VK =-")
		fmt.Println(" Insert Token (q - quit):")
		fmt.Scanln(&token)
		if token == "q" {
			os.Exit(0)
		}
		response, err := client.Get("https://api.vk.com/api.php?oauth=1&method=users.get&access_token=" + token + "&v=5.92")
		if err != nil {
			fmt.Println("Err client.Get: ", err, "\n************")
			break
		}
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Println("Err ioutil.readall: ***\n", err, "\n***")
			break
		}
		defer response.Body.Close()
		err = json.Unmarshal(contents, &vkUser)
		if err != nil {
			fmt.Println("**********\n", err, "**********")
		}
		fmt.Println(vkUser)
	}
}
